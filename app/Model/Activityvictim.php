<?php
App::uses('AppModel', 'Model');
/**
 * Activityvictim Model
 *
 * @property Victim $Victim
 * @property Activity $Activity
 * @property Volunteer $Volunteer
 */
class Activityvictim extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Victim' => array(
			'className' => 'Victim',
			'foreignKey' => 'victim_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Activity' => array(
			'className' => 'Activity',
			'foreignKey' => 'activity_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Volunteer' => array(
			'className' => 'Volunteer',
			'foreignKey' => 'volunteer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
