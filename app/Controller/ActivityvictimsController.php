<?php
App::uses('AppController', 'Controller');
/**
 * Activityvictims Controller
 *
 * @property Activityvictim $Activityvictim
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ActivityvictimsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Activityvictim->recursive = 0;
		$this->set('activityvictims', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Activityvictim->exists($id)) {
			throw new NotFoundException(__('Invalid activityvictim'));
		}
		$options = array('conditions' => array('Activityvictim.' . $this->Activityvictim->primaryKey => $id));
		$this->set('activityvictim', $this->Activityvictim->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Activityvictim->create();
			if ($this->Activityvictim->save($this->request->data)) {
				$this->Session->setFlash(__('The activityvictim has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The activityvictim could not be saved. Please, try again.'));
			}
		}
		$victims = $this->Activityvictim->Victim->find('list');
		$activities = $this->Activityvictim->Activity->find('list');
		$volunteers = $this->Activityvictim->Volunteer->find('list');
		$this->set(compact('victims', 'activities', 'volunteers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Activityvictim->exists($id)) {
			throw new NotFoundException(__('Invalid activityvictim'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Activityvictim->save($this->request->data)) {
				$this->Session->setFlash(__('The activityvictim has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The activityvictim could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Activityvictim.' . $this->Activityvictim->primaryKey => $id));
			$this->request->data = $this->Activityvictim->find('first', $options);
		}
		$victims = $this->Activityvictim->Victim->find('list');
		$activities = $this->Activityvictim->Activity->find('list');
		$volunteers = $this->Activityvictim->Volunteer->find('list');
		$this->set(compact('victims', 'activities', 'volunteers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Activityvictim->id = $id;
		if (!$this->Activityvictim->exists()) {
			throw new NotFoundException(__('Invalid activityvictim'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Activityvictim->delete()) {
			$this->Session->setFlash(__('The activityvictim has been deleted.'));
		} else {
			$this->Session->setFlash(__('The activityvictim could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
