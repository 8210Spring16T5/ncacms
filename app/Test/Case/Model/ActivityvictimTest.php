<?php
App::uses('Activityvictim', 'Model');

/**
 * Activityvictim Test Case
 *
 */
class ActivityvictimTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.activityvictim',
		'app.victim',
		'app.activity',
		'app.location',
		'app.staff',
		'app.volunteer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Activityvictim = ClassRegistry::init('Activityvictim');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Activityvictim);

		parent::tearDown();
	}

}
