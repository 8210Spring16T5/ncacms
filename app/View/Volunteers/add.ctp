<div class="volunteers form">
<?php echo $this->Form->create('Volunteer'); ?>
	<fieldset>
		<legend><?php echo __('Add Volunteer'); ?></legend>
	<?php
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('street_address');
		echo $this->Form->input('city');
		echo $this->Form->input('state');
		echo $this->Form->input('zip');
		echo $this->Form->input('email');
		echo $this->Form->input('work_phone');
		echo $this->Form->input('mobile_phone');
		echo $this->Form->input('notes');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Volunteers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Activities'), array('controller' => 'activities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activity'), array('controller' => 'activities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Activityvictims'), array('controller' => 'activityvictims', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activityvictim'), array('controller' => 'activityvictims', 'action' => 'add')); ?> </li>
	</ul>
</div>
