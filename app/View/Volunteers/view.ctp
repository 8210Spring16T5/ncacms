<div class="volunteers view">
<h2><?php echo __('Volunteer'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('First Name'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['first_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Name'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['last_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Street Address'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['street_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zip'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['zip']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Work Phone'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['work_phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mobile Phone'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['mobile_phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['notes']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Volunteer'), array('action' => 'edit', $volunteer['Volunteer']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Volunteer'), array('action' => 'delete', $volunteer['Volunteer']['id']), array(), __('Are you sure you want to delete # %s?', $volunteer['Volunteer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Volunteers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Volunteer'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Activities'), array('controller' => 'activities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activity'), array('controller' => 'activities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Activityvictims'), array('controller' => 'activityvictims', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activityvictim'), array('controller' => 'activityvictims', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Activities'); ?></h3>
	<?php if (!empty($volunteer['Activity'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Staff Id'); ?></th>
		<th><?php echo __('Volunteer Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Date'); ?></th>
		<th><?php echo __('Time'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Cost'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($volunteer['Activity'] as $activity): ?>
		<tr>
			<td><?php echo $activity['id']; ?></td>
			<td><?php echo $activity['location_id']; ?></td>
			<td><?php echo $activity['staff_id']; ?></td>
			<td><?php echo $activity['volunteer_id']; ?></td>
			<td><?php echo $activity['name']; ?></td>
			<td><?php echo $activity['date']; ?></td>
			<td><?php echo $activity['time']; ?></td>
			<td><?php echo $activity['description']; ?></td>
			<td><?php echo $activity['cost']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'activities', 'action' => 'view', $activity['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'activities', 'action' => 'edit', $activity['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'activities', 'action' => 'delete', $activity['id']), array(), __('Are you sure you want to delete # %s?', $activity['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Activity'), array('controller' => 'activities', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Activityvictims'); ?></h3>
	<?php if (!empty($volunteer['Activityvictim'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Victim Id'); ?></th>
		<th><?php echo __('Activity Id'); ?></th>
		<th><?php echo __('Volunteer Id'); ?></th>
		<th><?php echo __('Feedback'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($volunteer['Activityvictim'] as $activityvictim): ?>
		<tr>
			<td><?php echo $activityvictim['id']; ?></td>
			<td><?php echo $activityvictim['victim_id']; ?></td>
			<td><?php echo $activityvictim['activity_id']; ?></td>
			<td><?php echo $activityvictim['volunteer_id']; ?></td>
			<td><?php echo $activityvictim['feedback']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'activityvictims', 'action' => 'view', $activityvictim['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'activityvictims', 'action' => 'edit', $activityvictim['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'activityvictims', 'action' => 'delete', $activityvictim['id']), array(), __('Are you sure you want to delete # %s?', $activityvictim['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Activityvictim'), array('controller' => 'activityvictims', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
