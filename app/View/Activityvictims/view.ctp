<div class="activityvictims view">
<h2><?php echo __('Activityvictim'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($activityvictim['Activityvictim']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Victim'); ?></dt>
		<dd>
			<?php echo $this->Html->link($activityvictim['Victim']['id'], array('controller' => 'victims', 'action' => 'view', $activityvictim['Victim']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Activity'); ?></dt>
		<dd>
			<?php echo $this->Html->link($activityvictim['Activity']['name'], array('controller' => 'activities', 'action' => 'view', $activityvictim['Activity']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Volunteer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($activityvictim['Volunteer']['id'], array('controller' => 'volunteers', 'action' => 'view', $activityvictim['Volunteer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Feedback'); ?></dt>
		<dd>
			<?php echo h($activityvictim['Activityvictim']['feedback']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Activityvictim'), array('action' => 'edit', $activityvictim['Activityvictim']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Activityvictim'), array('action' => 'delete', $activityvictim['Activityvictim']['id']), array(), __('Are you sure you want to delete # %s?', $activityvictim['Activityvictim']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Activityvictims'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activityvictim'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Victims'), array('controller' => 'victims', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Victim'), array('controller' => 'victims', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Activities'), array('controller' => 'activities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activity'), array('controller' => 'activities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Volunteers'), array('controller' => 'volunteers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Volunteer'), array('controller' => 'volunteers', 'action' => 'add')); ?> </li>
	</ul>
</div>
