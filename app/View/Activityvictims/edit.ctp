<div class="activityvictims form">
<?php echo $this->Form->create('Activityvictim'); ?>
	<fieldset>
		<legend><?php echo __('Edit Activityvictim'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('victim_id');
		echo $this->Form->input('activity_id');
		echo $this->Form->input('volunteer_id');
		echo $this->Form->input('feedback');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Activityvictim.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Activityvictim.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Activityvictims'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Victims'), array('controller' => 'victims', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Victim'), array('controller' => 'victims', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Activities'), array('controller' => 'activities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activity'), array('controller' => 'activities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Volunteers'), array('controller' => 'volunteers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Volunteer'), array('controller' => 'volunteers', 'action' => 'add')); ?> </li>
	</ul>
</div>
