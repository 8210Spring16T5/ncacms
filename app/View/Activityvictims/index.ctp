<div class="activityvictims index">
	<h2><?php echo __('Activityvictims'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('victim_id'); ?></th>
			<th><?php echo $this->Paginator->sort('activity_id'); ?></th>
			<th><?php echo $this->Paginator->sort('volunteer_id'); ?></th>
			<th><?php echo $this->Paginator->sort('feedback'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($activityvictims as $activityvictim): ?>
	<tr>
		<td><?php echo h($activityvictim['Activityvictim']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($activityvictim['Victim']['id'], array('controller' => 'victims', 'action' => 'view', $activityvictim['Victim']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($activityvictim['Activity']['name'], array('controller' => 'activities', 'action' => 'view', $activityvictim['Activity']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($activityvictim['Volunteer']['id'], array('controller' => 'volunteers', 'action' => 'view', $activityvictim['Volunteer']['id'])); ?>
		</td>
		<td><?php echo h($activityvictim['Activityvictim']['feedback']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $activityvictim['Activityvictim']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $activityvictim['Activityvictim']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $activityvictim['Activityvictim']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $activityvictim['Activityvictim']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Activityvictim'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Victims'), array('controller' => 'victims', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Victim'), array('controller' => 'victims', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Activities'), array('controller' => 'activities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activity'), array('controller' => 'activities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Volunteers'), array('controller' => 'volunteers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Volunteer'), array('controller' => 'volunteers', 'action' => 'add')); ?> </li>
	</ul>
</div>
