<div class="victims view">
<h2><?php echo __('Victim'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($victim['Victim']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('First Name'); ?></dt>
		<dd>
			<?php echo h($victim['Victim']['first_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Name'); ?></dt>
		<dd>
			<?php echo h($victim['Victim']['last_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Street Address'); ?></dt>
		<dd>
			<?php echo h($victim['Victim']['street_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($victim['Victim']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($victim['Victim']['state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zip'); ?></dt>
		<dd>
			<?php echo h($victim['Victim']['zip']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($victim['Victim']['phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Disease Type'); ?></dt>
		<dd>
			<?php echo h($victim['Victim']['disease_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($victim['Victim']['notes']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Victim'), array('action' => 'edit', $victim['Victim']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Victim'), array('action' => 'delete', $victim['Victim']['id']), array(), __('Are you sure you want to delete # %s?', $victim['Victim']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Victims'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Victim'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Activityvictims'), array('controller' => 'activityvictims', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activityvictim'), array('controller' => 'activityvictims', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Activityvictims'); ?></h3>
	<?php if (!empty($victim['Activityvictim'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Victim Id'); ?></th>
		<th><?php echo __('Activity Id'); ?></th>
		<th><?php echo __('Volunteer Id'); ?></th>
		<th><?php echo __('Feedback'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($victim['Activityvictim'] as $activityvictim): ?>
		<tr>
			<td><?php echo $activityvictim['id']; ?></td>
			<td><?php echo $activityvictim['victim_id']; ?></td>
			<td><?php echo $activityvictim['activity_id']; ?></td>
			<td><?php echo $activityvictim['volunteer_id']; ?></td>
			<td><?php echo $activityvictim['feedback']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'activityvictims', 'action' => 'view', $activityvictim['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'activityvictims', 'action' => 'edit', $activityvictim['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'activityvictims', 'action' => 'delete', $activityvictim['id']), array(), __('Are you sure you want to delete # %s?', $activityvictim['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Activityvictim'), array('controller' => 'activityvictims', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
