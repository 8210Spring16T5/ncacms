<div class="victims index">
	<h2><?php echo __('Victims'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('first_name'); ?></th>
			<th><?php echo $this->Paginator->sort('last_name'); ?></th>
			<th><?php echo $this->Paginator->sort('street_address'); ?></th>
			<th><?php echo $this->Paginator->sort('city'); ?></th>
			<th><?php echo $this->Paginator->sort('state'); ?></th>
			<th><?php echo $this->Paginator->sort('zip'); ?></th>
			<th><?php echo $this->Paginator->sort('phone'); ?></th>
			<th><?php echo $this->Paginator->sort('disease_type'); ?></th>
			<th><?php echo $this->Paginator->sort('notes'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($victims as $victim): ?>
	<tr>
		<td><?php echo h($victim['Victim']['id']); ?>&nbsp;</td>
		<td><?php echo h($victim['Victim']['first_name']); ?>&nbsp;</td>
		<td><?php echo h($victim['Victim']['last_name']); ?>&nbsp;</td>
		<td><?php echo h($victim['Victim']['street_address']); ?>&nbsp;</td>
		<td><?php echo h($victim['Victim']['city']); ?>&nbsp;</td>
		<td><?php echo h($victim['Victim']['state']); ?>&nbsp;</td>
		<td><?php echo h($victim['Victim']['zip']); ?>&nbsp;</td>
		<td><?php echo h($victim['Victim']['phone']); ?>&nbsp;</td>
		<td><?php echo h($victim['Victim']['disease_type']); ?>&nbsp;</td>
		<td><?php echo h($victim['Victim']['notes']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $victim['Victim']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $victim['Victim']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $victim['Victim']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $victim['Victim']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Victim'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Activityvictims'), array('controller' => 'activityvictims', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activityvictim'), array('controller' => 'activityvictims', 'action' => 'add')); ?> </li>
	</ul>
</div>
