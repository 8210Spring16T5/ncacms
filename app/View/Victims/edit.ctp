<div class="victims form">
<?php echo $this->Form->create('Victim'); ?>
	<fieldset>
		<legend><?php echo __('Edit Victim'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('street_address');
		echo $this->Form->input('city');
		echo $this->Form->input('state');
		echo $this->Form->input('zip');
		echo $this->Form->input('phone');
		echo $this->Form->input('disease_type');
		echo $this->Form->input('notes');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Victim.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Victim.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Victims'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Activityvictims'), array('controller' => 'activityvictims', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activityvictim'), array('controller' => 'activityvictims', 'action' => 'add')); ?> </li>
	</ul>
</div>
