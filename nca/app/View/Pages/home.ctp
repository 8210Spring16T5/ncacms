<!DOCTYPE html>
<html lang="en">
<head>
<title> Staff Home</title>
<script>
 $(document).ready(function(){

  $().UItoTop({ easingType: 'easeOutQuart' });
  $('#stuck_container').tmStickUp({});
  $('.gallery .gall_item').touchTouch();

  }); 
</script>
<!--[if lt IE 9]>
 <div style=' clear: both; text-align:center; position: relative;'>
   <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
     <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
   </a>
</div>
<script src="js/html5shiv.js"></script>
<link rel="stylesheet" media="screen" href="css/ie.css">


<![endif]-->
</head>

<body class="page1" id="top">
<!--==============================
              header
=================================-->
<header>
<!--==============================
            Stuck menu
=================================-->
  <section id="stuck_container">
    <div class="container">
      <div class="row">
        <div class="grid_12">
        <h1>
          <a href="index.html">
            <img src="images/logo.png" alt="Logo alt">
          </a>
        </h1>  
          <div class="navigation">
            <nav>
              <br>
              Staff Menu

              <br>
          <br>
              <ul class="sf-menu">
               <li><a href="https://ncacms-8210spring16t5.rhcloud.com/ncacms2/">Home</a></li>
               <li><a href="https://ncacms-8210spring16t5.rhcloud.com/ncacms2/volunteers">Volunteers</a></li>
               <li><a href="https://ncacms-8210spring16t5.rhcloud.com/ncacms2/staffs">Staff</a></li>
               <li><a href="https://ncacms-8210spring16t5.rhcloud.com/ncacms2/activities">Activities</a></li>
               <li><a href="https://ncacms-8210spring16t5.rhcloud.com/ncacms2/activities/add">New Activities</a></li>
               <li><a href="https://ncacms-8210spring16t5.rhcloud.com/ncacms2/locations/add">Add Location</a></li>
               <li><a href="https://ncacms-8210spring16t5.rhcloud.com/nca/users/logout">Log out</a></li>
             </ul>
            </nav>        
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
  </section> 
</header>        

</body>
</html>

